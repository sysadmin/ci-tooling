#!/usr/bin/python3
import os
import sys
import tarfile
import tempfile
import argparse
from helperslib import BuildSpecs, BuildSystem, CommonUtils, EnvironmentHandler, Packages

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to install a project, diverting the installation for later capture if requested.')
parser.add_argument('--product', type=str, required=True)
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--environment', type=str, required=True)
parser.add_argument('--installedTo', type=str, required=True)
parser.add_argument('--divertedTo', type=str, required=True)
arguments = parser.parse_args()

# Create a temporary file, then open the file as a tar archive for writing
# We don't want it to be deleted as storePackage will move the archive into it's cache
archiveFile = tempfile.NamedTemporaryFile(delete=False)
archive = tarfile.open( fileobj=archiveFile, mode='w' )

# Now determine the path we should be archiving
# Because we could potentially be running on Windows we have to ensure our second path has been converted to a suitable form
# This conversion is necessary as os.path.join can't handle the presence of drive letters in paths other than the first argument
pathToArchive = os.path.join( arguments.divertedTo, CommonUtils.makePathRelative(arguments.installedTo) )

# Add all the files which need to be in the archive into the archive
# We want to capture the tree as it is inside the install directory and don't want any trailing slashes in the archive as this isn't standards compliant
# Therefore we list everything in the install directory and add each of those to the archive, rather than adding the whole install directory
filesToInclude = os.listdir( pathToArchive )
for filename in filesToInclude:
	fullPath = os.path.join(pathToArchive, filename)
	archive.add( fullPath, arcname=filename, recursive=True )

# Close the archive, which will write it out to disk, finishing what we need to do here
# This is also necessary on Windows to allow for storePackage to move it to it's final home
archive.close()
archiveFile.close()

# Initialize the archive manager
ourArchive = Packages.Archive( arguments.environment, arguments.platform )

# Determine which SCM revision we are storing
# This will be embedded into the package metadata, and later used for helpful output by prepare-dependencies.py
# GIT_COMMIT is set by Jenkins Git plugin, so we can rely on that for most of our builds
scmRevision = ''
if os.getenv('GIT_COMMIT') != '':
	scmRevision = os.getenv('GIT_COMMIT')

# Determine the package name...
package = Packages.nameForProject( arguments.product, arguments.project, arguments.branchGroup )

# Add the package to the archive
ourArchive.storePackage( package, archiveFile.name, scmRevision )

# Now open the archive - so we can extract it's contents over the install prefix
# This is so later tests can rely on the project having been installed
# Because storePackage will have moved our temporary file to the cache we have to ask the package archive where the file now lives
filename, metadata = ourArchive.retrievePackage( package )
archive = tarfile.open( name=filename, mode='r' )
archive.extractall( path=arguments.installedTo )

# All done!
sys.exit(0)
