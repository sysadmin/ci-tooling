import os
import yaml
from helperslib import CommonUtils

# Class to make it convenient to read configs for working with build specs
class Loader(object):

	# Loads the Project's configuration, should it have one and merges it with the default configuration
	def __init__(self, product, project, branchGroup, platform):

		# Start by copying the default settings
		self.mergedConfig = self.defaultOptions()

		# As an initial starting point, load any configuration options for this whole Product
		# These should be located at /build-specs/<Product>.yaml
		configFileLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'build-specs', product + '.yaml' )
		self.loadSpecificationFile( configFileLocation, branchGroup )

		# We should also look for Platform specific options for this Product
		configFileLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'build-specs', product + '-' + platform + '.yaml' )
		self.loadSpecificationFile( configFileLocation, branchGroup )

		# Where should our configuration file be for this specific build?
		# It should be at /build-specs/<Product>/<project>.yaml
		# As the configuration location already contains /build-specs/ we just have to add on <product> and <project>.yaml
		configFileLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'build-specs', product, project + '.yaml' )
		self.loadSpecificationFile( configFileLocation, branchGroup )

		# As a secondary option, we also allow for Platform specific build specs
		# These will be located at /build-specs/<Product>/<project>-<platform>.yaml
		# Let's make sure we load it as well
		configFileLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'build-specs', product, project + '-' + platform + '.yaml' )
		self.loadSpecificationFile( configFileLocation, branchGroup )

	# Allow fetching our config
	def __getitem__(self, name):
		# Do we know about this attribute?
		if name in self.mergedConfig:
			# Then return it
			return self.mergedConfig[name]

		# We don't know about it
		raise KeyError

	# Load the given specification file
	def loadSpecificationFile(self, configFileLocation, branchGroup):
		# Does the file exist?
		# If it does not, then we don't need to do anything else
		if not os.path.isfile(configFileLocation):
			return

		# Load the file now
		with open(configFileLocation, 'r') as configFile:
			# Parse the YAML file
			projectConfig = yaml.safe_load(configFile)

			# Does it specify something for this branch group?
			if branchGroup in projectConfig:
				# The merge it in
				self.mergedConfig.update( projectConfig[branchGroup] )

	# Specifies our defaults
	def defaultOptions(self):
		return {
			'in-source-build': False,
			'detect-build-system': True,
			'cmake-options': '',
			'autotools-options': '',
			'configure-commands': {},
                        'externalDependencies': '',
			'do-appstream-check': True,
			'run-tests': True,
			'per-test-timeout': 600,
			'setup-x-environment': True,
			'launch-dbus-session': True,
			'force-inject-asan': False,
			'ctest-arguments': '',
			'run-cppcheck': True,
			'cppcheck-arguments': '',
			'extract-lcov-results': True,
			'lcov-extractor-arguments': ''
		}
